let m = +prompt('Input minimal number');
while (m == null || m == "" || isNaN(m)) {
  m = +prompt('Incorrect format, please, input minimal number');
}

let n = +prompt('Input maximal number');
while (n == null || n == "" || isNaN(n)) {
  n = +prompt('Incorrect format, please, input maximal number');
}  

Cycle:
for (let i = m; i <= n; i++) {

  for (let j = 2; j < i; j++) { 
    if (i % j == 0) continue Cycle;
  }

  console.log( i );
}